import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
//Import the bootstrap Css
import 'bootstrap/dist/css/bootstrap.min.css';
//import AppNavbar from './components/AppNavbar';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

