import React from 'react';

//Dito natin isasave ung kung ano man ung ipapasa natinna values
//Create a context object. A context objec as the name states is a data type of an object that can be used to store info that can be shared to other components within the app
//The context object is a different approach to passing info between components and allow easier access by avoiding the use of prop-drilling

const UserContext = React.createContext();

//The "Provider component allows the other components to consume /use the context object and supply the necessary info needed to the context obj"

export const UserProvider = UserContext.Provider;

export default UserContext; 