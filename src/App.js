import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import CourseView from './pages/CourseView';
import Home from './pages/Home';
import Courses from './pages/Courses'
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/Error';
import './App.css';
import {UserProvider} from './UserContext';


function App() {

//React Context is nothing but a global state to the app. It is a way to make a particular data available to all components no matter how thet are nested.

//State hook for the user state that defines here for a global scope
//Initialized as an obj. with prop from the local Storage
//This will be used to store the user info and will be used for validating if a user is logged in on the app or not
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })
  //Function for clearing the localStorage on logout

  const unsetUser = () => {
    localStorage.clear()
  }
//useEffect nag tatake ng effect at least once pag nagload ung component. may function at optional dependency array as parameters. Pag may value ung dpendency array once lang magrurun ung component
  useEffect( () => {

    fetch("http://localhost:4000/api/users/details" , {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {
        setUser ({
          _id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  }, [])

  return (
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path ="/" element ={<Home />} />
            <Route path ="/courses" element ={<Courses />} />
            <Route path ="/courses/:courseId" element ={<CourseView />} />
            <Route path ="/login" element ={<Login />} />
            <Route path ="/register" element ={<Register />} />
            <Route path ="/logout" element ={<Logout />} />
            <Route path='*' element={<ErrorPage/>}   />
          </Routes>
        </Container>  
      </Router>
    </UserProvider>

  );
}

export default App;
